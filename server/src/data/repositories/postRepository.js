import { Op } from 'sequelize';
import sequelize from '../db/connection';
import {
  PostModel, CommentModel, UserModel, ImageModel, PostReactionModel,
  CommentReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;
const likeCaseComments = bool => `CASE WHEN "comments->commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      notFromUser
    } = filter;

    const where = { isDeleted: false };
    if (userId) {
      Object.assign(where, { userId: notFromUser === 'true' ? { [Op.ne]: userId } : userId });
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "isDeleted" = false AND "post"."id" = "comment"."postId")`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id',
        'comments->commentReactions.id'
      ],
      where: { id, isDeleted: false },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "isDeleted" = false AND "post"."id" = "comment"."postId")`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        include: [{
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }, {
          model: CommentReactionModel,
          attributes: [[sequelize.fn('SUM', sequelize.literal(likeCaseComments(true))), 'likeCountComments'],
            [sequelize.fn('SUM', sequelize.literal(likeCaseComments(false))), 'dislikeCountComments']],
          group: [
            'comments->commentReactions.id'
          ]
        }]
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }]
    });
  }

  deleteByPostIdAndUserId(id, userId) {
    return this.model.update({
      isDeleted: true
    }, {
      where: {
        id,
        userId
      }
    });
  }

  updateByPostIdAndUserId(id, userId, data) {
    return this.model.update(data, {
      where: {
        id,
        userId
      }
    });
  }

  getUserByPostId(id) {
    return this.model.findOne({
      where: { id },
      include: [{
        model: UserModel,
        attributes: ['email']
      }]
    });
  }
}

export default new PostRepository(PostModel);
