module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn('comments', 'isDeleted', {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
    allowNull: false
  }),

  down: queryInterface => queryInterface.removeColumn('comments', 'isDeleted')
};
