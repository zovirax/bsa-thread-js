module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn('posts', 'isDeleted', {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
    allowNull: false
  }),

  down: queryInterface => queryInterface.removeColumn('posts', 'isDeleted')
};
