import env from '../env';

const resolveLink = endpoint => `${env.frontEnd.host}${endpoint}`;

export const getShareLink = postId => resolveLink(`/share/${postId}`);
