import env from '../env';
import HttpError from './types/HttpError';

const nodemailer = require('nodemailer');

const { user, password: pass } = env.email;

const createTransporter = () => nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  auth: {
    user,
    pass
  }
});

const sendEmail = async (to, subject, text) => {
  const transporter = await createTransporter();
  try {
    await transporter.sendMail({
      from: user,
      to,
      subject,
      text
    });
  } catch (err) {
    throw new HttpError(`Could not send an e-mail: ${err.message}`, 500);
  }
};

export const sharePost = async (email, username, link) => {
  await sendEmail(email,
    'Somebody shared a post with you!',
    `Hello.\n\n${username} shared a post with you.\n\nHave a look: ${link}`);
};

export const likedPost = async (email, username, link) => {
  await sendEmail(email,
    'Somebody liked your post!',
    `Hello.\n\nYour post was liked by ${username}.\n\nPost: ${link}`);
};
