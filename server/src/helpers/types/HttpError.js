module.exports = class HttpError extends Error {
  constructor(message, status) {
    if (status < 400 || status > 599) {
      throw new Error('No valid http status, should be 400 - 599');
    }
    super(message || 'Internal Server Error');
    this.name = 'HttpError';
    this.status = status || 500;
  }

  static notFound(message) {
    return new HttpError(message || 'Not Found', 404);
  }
};
