import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import { notFound } from '../../helpers/types/HttpError';
import { getUserById, getUserByUsername } from './userService';
import * as emailHelper from '../../helpers/emailHelper';
import { getShareLink } from '../../helpers/urlHelper';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = async id => {
  const post = await postRepository.getPostById(id);
  return post || Promise.reject(notFound('Post not found'));
};

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const isToggled = reaction && reaction.isLike !== isLike;

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : {
    postReaction: await postReactionRepository.getPostReaction(userId, postId),
    isToggled
  };
};

export const deletePost = (postId, userId) => postRepository.deleteByPostIdAndUserId(postId, userId);

export const updatePost = async (userId, id, { body, imageId }) => {
  await postRepository.updateByPostIdAndUserId(id, userId, {
    body,
    imageId
  });
  return getPostById(id);
};

export const sharePostByEmail = async (postId, usernames = [], userId) => {
  const { username: senderUsername } = await getUserById(userId);
  const shareLink = getShareLink(postId);

  const users = await Promise.all(
    (usernames instanceof Array ? usernames : [usernames])
      .map(getUserByUsername)
  );

  await Promise.all(
    users.map(({ email }) => email && emailHelper.sharePost(email, senderUsername, shareLink))
  );
};

export const notifyLikedPostByEmail = async (postId, userId) => {
  const { username: likedByUsername } = await getUserById(userId);
  const shareLink = getShareLink(postId);
  const { user: { email } } = await postRepository.getUserByPostId(postId);
  await emailHelper.likedPost(email, likedByUsername, shareLink);
};

export const getUsersLikedPost = async postId => {
  const reactions = await postReactionRepository.getUsersIdLiked(postId);
  return Promise.all(reactions.map(r => getUserById(r.userId)));
};
