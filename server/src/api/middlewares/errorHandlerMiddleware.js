export default (err, req, res, next) => {
  if (res.headersSent) { // http://expressjs.com/en/guide/error-handling.html
    next(err);
  } else {
    const { status = 500 } = err;
    let { message = '' } = err;

    if (process.env.NODE_ENV === 'production' && status === 500) {
      // Hide message due to the security issues
      message = 'Internal Server Error. Please contact a system administrator';
    }

    res.status(status).send({ status, message });
  }
};
