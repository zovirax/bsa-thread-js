import { Router } from 'express';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/share', (req, res, next) => {
    const { postId, usernames } = req.query;
    postService.sharePostByEmail(postId, usernames, req.user.id)
      .then(() => res.sendStatus(204))
      .catch(next);
  })
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .get('/:id/users-liked', (req, res, next) => postService.getUsersLikedPost(req.params.id)
    .then(users => res.send(users))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(reaction => {
      const { postReaction } = reaction;
      if (postReaction && (postReaction.post.userId !== req.user.id) && postReaction.isLike) {
        // notify a user if someone (not himself) liked his post
        req.io.to(postReaction.post.userId).emit('like', 'Your post was liked!');
        postService.notifyLikedPostByEmail(postReaction.post.id, req.user.id);
      }
      return res.send(reaction);
    })
    .catch(next))
  .delete('/:id', (req, res, next) => postService.deletePost(req.params.id, req.user.id)
    .then(() => res.sendStatus(204))
    .catch(next))
  .put('/:id', (req, res, next) => postService.updatePost(req.user.id, req.params.id, req.body.post)
    .then(post => res.send(post))
    .catch(next));

export default router;
