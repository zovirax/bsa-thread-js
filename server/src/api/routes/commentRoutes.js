import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(reaction => res.send(reaction))
    .catch(next))
  .put('/:id', (req, res, next) => commentService.updateComment(req.user.id, req.params.id, req.body.comment)
    .then(() => res.sendStatus(204))
    .catch(next))
  .delete('/:id', (req, res, next) => commentService.deleteComment(req.params.id, req.user.id)
    .then(() => res.sendStatus(204))
    .catch(next));

export default router;
