import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  DELETE_POST,
  EDIT_POST,
  DELETE_COMMENT
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const editPostAction = post => ({
  type: EDIT_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const deletePostAction = postId => ({
  type: DELETE_POST,
  postId
});

const deleteCommentAction = commentId => ({
  type: DELETE_COMMENT,
  commentId
});

export const editPost = post => async dispatch => {
  const updated = await postService.updatePost(post);
  dispatch(editPostAction(updated));
};

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  posts.map(async p => ({ ...p, usersLiked: await postService.getUsersLikedPost(p.id) }));
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));

  const mappedPosts = await Promise.all(filteredPosts
    .map(async p => ({ ...p, usersLiked: await postService.getUsersLikedPost(p.id) }))); // load users, who liked post

  dispatch(addMorePostsAction(mappedPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const deletePost = postId => async dispatch => {
  await postService.deletePost(postId);
  dispatch(deletePostAction(postId));
};

export const deleteComment = commentId => async dispatch => {
  await commentService.deleteComment(commentId);
  dispatch(deleteCommentAction(commentId));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { postReaction, isToggled } = await postService.likePost(postId);
  const diff = postReaction ? 1 : -1; // if postReaction exists then the post was liked, otherwise - like was removed
  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
    dislikeCount: isToggled ? post.dislikeCount - 1 : post.dislikeCount
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { postReaction, isToggled } = await postService.dislikePost(postId);
  // if postReaction exists then the post was disliked, otherwise - dislike was removed
  const diff = postReaction ? 1 : -1;
  const mapDislikes = post => ({
    ...post,
    likeCount: isToggled ? post.likeCount - 1 : post.likeCount, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + diff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const { commentReaction, isToggled } = await commentService.dislikeComment(commentId);
  const diff = commentReaction ? 1 : -1;

  const mapDislikes = comment => ({
    ...comment,
    likeCount: isToggled ? comment.likeCount - 1 : comment.likeCount,
    dislikeCount: Number(comment.dislikeCount) + diff
  });

  const { posts: { expandedPost: { comments } } } = getRootState();
  const updatedComments = comments.map(comment => (comment.id !== commentId ? comment : mapDislikes(comment)));

  dispatch(setExpandedPostAction({ ...getRootState().posts.expandedPost, comments: updatedComments }));
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const { commentReaction, isToggled } = await commentService.likeComment(commentId);
  const diff = commentReaction ? 1 : -1;

  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diff,
    dislikeCount: isToggled ? comment.dislikeCount - 1 : comment.dislikeCount
  });

  const { posts: { expandedPost: { comments } } } = getRootState();
  const updatedComments = comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));

  dispatch(setExpandedPostAction({ ...getRootState().posts.expandedPost, comments: updatedComments }));
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
