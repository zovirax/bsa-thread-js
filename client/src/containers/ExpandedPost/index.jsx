import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import { likePost, toggleExpandedPost, addComment, dislikePost, deletePost, likeComment,
  dislikeComment, deleteComment } from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  sharePost,
  likePost: like,
  toggleExpandedPost: toggle,
  addComment: add,
  dislikePost: dislike,
  deletePost: deleteById,
  shareByEmail,
  likeComment: likeCommentById,
  dislikeComment: dislikeCommentById,
  deleteComment: deleteCommentById,
  userId
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Post
            post={post}
            likePost={like}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            dislikePost={dislike}
            deletePost={deleteById}
            shareByEmail={shareByEmail}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
              .map(comment => (
                comment.isDeleted ? null : (
                  <Comment
                    likeComment={likeCommentById}
                    dislikeComment={dislikeCommentById}
                    key={comment.id}
                    isOwner={userId === comment.user.id}
                    comment={comment}
                    deleteComment={deleteCommentById}
                  />
                )
              ))}
            <AddComment postId={post.id} addComment={add} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  shareByEmail: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  userId: PropTypes.string,
  deleteComment: PropTypes.func.isRequired
};

ExpandedPost.defaultProps = {
  userId: undefined
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = { likePost,
  toggleExpandedPost,
  addComment,
  dislikePost,
  deletePost,
  likeComment,
  dislikeComment,
  deleteComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
