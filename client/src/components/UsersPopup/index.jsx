import React from 'react';
import { Popup } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import Spinner from '../Spinner';

const UsersPopup = ({
  trigger,
  header,
  users
}) => (
  <Popup trigger={trigger}>
    <Popup.Header>{header}</Popup.Header>
    <Popup.Content>
      {users || <Spinner />}
    </Popup.Content>
  </Popup>
);

UsersPopup.propTypes = {
  trigger: PropTypes.objectOf(PropTypes.any).isRequired,
  users: PropTypes.objectOf(PropTypes.any).isRequired,
  header: PropTypes.string.isRequired
};

export default UsersPopup;
