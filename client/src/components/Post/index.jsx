// eslint-disable-next-line no-unused-vars
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Popup } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';
import Spinner from '../Spinner';

// eslint-disable-next-line max-len,no-unused-vars
const Post = ({ post, likePost, toggleExpandedPost, sharePost, deletePost, dislikePost, editPost, shareByEmail }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    isOwner,
    usersLiked
  } = post;
  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Popup trigger={(
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
            <Icon name="thumbs up" />
            {likeCount}
          </Label>
        )}
        >
          <Popup.Header>Likes</Popup.Header>
          <Popup.Content>
            {(usersLiked && usersLiked.map(u => u.username).join(', ')) || <Spinner />}
          </Popup.Content>
        </Popup>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => shareByEmail(id)}>
          <Icon name="mail" title="Share by email" />
        </Label>
        {isOwner && (
          <>
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => editPost(post)}>
              <Icon name="edit" title="Edit" />
            </Label>
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deletePost(id)}>
              <Icon name="remove" title="Delete" />
            </Label>
          </>
        )}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func,
  dislikePost: PropTypes.func.isRequired,
  editPost: PropTypes.func,
  shareByEmail: PropTypes.func.isRequired
};

Post.defaultProps = {
  deletePost: () => {},
  editPost: () => {}
};

export default Post;
