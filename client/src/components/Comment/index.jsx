import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

// eslint-disable-next-line max-len
const Comment = ({ comment: { id, body, createdAt, user, likeCount, dislikeCount }, likeComment, dislikeComment, deleteComment, isOwner }) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={getUserImgLink(user.image)} />
    <CommentUI.Content>
      <CommentUI.Author as="a">
        {user.username}
      </CommentUI.Author>
      <CommentUI.Metadata>
        {moment(createdAt).fromNow()}
      </CommentUI.Metadata>
      <CommentUI.Text>
        {body}
      </CommentUI.Text>
      <CommentUI.Actions>
        <CommentUI.Action as="span">
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likeComment(id)}>
            <Icon name="thumbs up" />
            {+likeCount || 'Be first'}
          </Label>
        </CommentUI.Action>
        <CommentUI.Action as="span">
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikeComment(id)}>
            <Icon name="thumbs down" />
            {+dislikeCount || 'Be first'}
          </Label>
        </CommentUI.Action>
        {isOwner && (
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deleteComment(id)}>
            <Icon name="remove" title="Delete" />
          </Label>
        )}
      </CommentUI.Actions>
    </CommentUI.Content>
  </CommentUI>
);

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  isOwner: PropTypes.bool.isRequired,
  deleteComment: PropTypes.func.isRequired
};

export default Comment;
