import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Button, Image, Icon, Segment } from 'semantic-ui-react';
import styles from '../AddPost/styles.module.scss';

const EditPost = ({
  post,
  editPost,
  close,
  uploadImage
}) => {
  const [body, setBody] = useState(post.body);
  const [image, setImage] = useState(post.imageId);
  const [isUploading, setIsUploading] = useState(false);

  const handleEditPost = async () => {
    if (!body || !body.trim()) {
      return;
    }
    await editPost({ ...post, imageId: image?.imageId, body });
    setBody('');
    setImage(undefined);
    close();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={close}>
      <Modal.Content>
        <Segment>
          <Form onSubmit={handleEditPost}>
            <Form.TextArea
              name="body"
              value={body}
              placeholder="Edit this post..."
              onChange={ev => setBody(ev.target.value)}
            />
            {image?.imageLink && (
              <div className={styles.imageWrapper}>
                <Image className={styles.image} src={image?.imageLink} alt="post" />
              </div>
            )}
            <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
              <Icon name="image" />
              Attach image
              <input name="image" type="file" onChange={handleUploadFile} hidden />
            </Button>
            <Button floated="right" color="blue" type="submit" disabled={isUploading}>Edit</Button>
          </Form>
        </Segment>
      </Modal.Content>
    </Modal>
  );
};

EditPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  editPost: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default EditPost;
