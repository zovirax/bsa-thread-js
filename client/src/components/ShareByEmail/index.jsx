import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Button } from 'semantic-ui-react';

const ShareByEmail = ({
  id: postId,
  sendByEmail,
  close
}) => {
  const [usernames, setUsernames] = useState('');

  const handleSharePost = () => {
    if (!usernames || !usernames.trim()) {
      return;
    }
    sendByEmail(usernames.split(', '), postId);
    close();
    setUsernames('');
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={close}>
      <Modal.Content>
        <Form reply onSubmit={handleSharePost}>
          <Form.TextArea
            name="usernames"
            value={usernames}
            placeholder="Enter usernames, for example, Elon Musk, Trump, avocado..."
            onChange={ev => setUsernames(ev.target.value)}
          />
          <Button type="submit" content="Share by email" labelPosition="left" icon="edit" primary />
        </Form>
      </Modal.Content>
    </Modal>
  );
};

ShareByEmail.propTypes = {
  id: PropTypes.string.isRequired,
  sendByEmail: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default ShareByEmail;
