import callWebApi from 'src/helpers/webApiHelper';

export const getAllPosts = async filter => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'GET',
    query: filter
  });
  return response.json();
};

export const addPost = async request => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'POST',
    request
  });
  return response.json();
};

export const getPost = async id => {
  // TODO: refactor (move mapping function to a helper)
  const mapComments = comments => {
    const mapped = [];
    // eslint-disable-next-line no-restricted-syntax
    for (const comment of comments) {
      comment.likeCount = 0;
      comment.dislikeCount = 0;
      // eslint-disable-next-line no-restricted-syntax
      for (const reaction of comment.commentReactions) {
        comment.likeCount += (+reaction.likeCountComments || 0);
        comment.dislikeCount += (+reaction.dislikeCountComments || 0);
      }
      mapped.push(comment);
    }
    return mapped;
  };

  const response = await callWebApi({
    endpoint: `/api/posts/${id}`,
    type: 'GET'
  });

  const post = await response.json();
  const mappedComments = mapComments(post.comments);
  return { ...post, comments: mappedComments };
};

const reactToPost = async (postId, isLike) => {
  const response = await callWebApi({
    endpoint: '/api/posts/react',
    type: 'PUT',
    request: {
      postId,
      isLike
    }
  });
  return response.json();
};

export const likePost = postId => reactToPost(postId, true);

export const dislikePost = postId => reactToPost(postId, false);

export const deletePost = postId => {
  callWebApi({
    endpoint: `/api/posts/${postId}`,
    type: 'DELETE'
  });
};

export const updatePost = async post => {
  const response = await callWebApi({
    endpoint: `/api/posts/${post.id}`,
    type: 'PUT',
    request: { post }
  });
  return response.json();
};

export const sendByEmail = (usernames, postId) => {
  callWebApi({
    endpoint: '/api/posts/share',
    type: 'GET',
    query: {
      usernames,
      postId
    }
  });
};

export const getUsersLikedPost = async postId => {
  const response = await callWebApi({
    endpoint: `/api/posts/${postId}/users-liked`,
    type: 'GET'
  });

  return response.json();
};

// should be replaced by approppriate function
export const getPostByHash = async hash => getPost(hash);
